const chai = require("chai");
const chaiHttp = require("chai-http");
const chaiDom = require("chai-dom");
const { JSDOM } = require("jsdom");
const server = require("../server");
chai.use(chaiHttp);
chai.use(chaiDom);

describe("Weather app", () => {
  describe("get /", () => {
    it("should render index page", done => {
      chai
        .request(server)
        .get("/")
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.html;
          const { document } = new JSDOM(res.text).window;
          document
            .querySelector("input[name=city]")
            .should.have.attr("placeholder", "Enter a City");
          done();
        });
    });
  });

  describe("post /", () => {
    it("should display an error for invalid input", async () => {
      for (let city of ["test", "", "asdfsaf", "Msk"]) {
        await chai
          .request(server)
          .post("/")
          .set("content-type", "application/x-www-form-urlencoded")
          .send({ city: city })
          .then(res => {
            res.should.have.status(200);
            res.should.be.html;
            const { document } = new JSDOM(res.text).window;
            const message = document.getElementById("msg");
            message.should.exist;
            message.innerHTML.should.equal("Error, please try again");
          });
      }
    });

    it("should display a temperature in required city", async () => {
      for (let city of [
        "Moscow",
        "London",
        "Tokyo",
        "Portland",
        "Mexico",
        "Paris"
      ]) {
        await chai
          .request(server)
          .post("/")
          .set("content-type", "application/x-www-form-urlencoded")
          .send({ city: city })
          .then(res => {
            res.should.have.status(200);
            res.should.be.html;
            const { document } = new JSDOM(res.text).window;
            const message = document.getElementById("msg");
            message.should.exist;
            message.innerHTML.should.have.string(city);
          });
      }
    });
  });
});
